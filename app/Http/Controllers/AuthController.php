<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\User;
use Illuminate\Support\Facades\Auth;

class AuthController extends Controller
{
    public function handleTelegramCallback(Request $request)
    {
        $data = $request->all();

        if ($this->validateTelegramAuthData($data)) {
            $user = User::updateOrCreate(
                ['id' => $data['id']],
                [
                    'first_name' => $data['first_name'],
                    'last_name' => $data['last_name'] ?? null,
                    'username' => $data['username'] ?? null,
                    'photo_url' => $data['photo_url'] ?? null,
                    'auth_date' => date('Y-m-d H:i:s', $data['auth_date']),
                ]
            );

            Auth::login($user);
            return redirect()->route('home');
        } else {
            return redirect()->route('login')->with('error', 'Неизвестная ошибка :(');
        }
    }

    public function logout(Request $request)
    {
        Auth::logout();
        $request->session()->invalidate();
        $request->session()->regenerateToken();

        return redirect('/');
    }

    private function validateTelegramAuthData($data)
    {
        $checkHash = $data['hash'];
        unset($data['hash']);

        $dataCheckString = collect($data)
            ->map(function ($value, $key) {
                return "$key=$value";
            })
            ->sort()
            ->join("\n");

        $secretKey = hash('sha256', env('TELEGRAM_BOT_TOKEN'), true);
        $hash = hash_hmac('sha256', $dataCheckString, $secretKey);

        return $hash === $checkHash;
    }
}

