<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class Coil extends Model
{
    protected $fillable = [
        'name',
        'resistance',
        'cost',
    ];

    public function device(): BelongsTo
    {
        return $this->belongsTo(Device::class);
    }
}
