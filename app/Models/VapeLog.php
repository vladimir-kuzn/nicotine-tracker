<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class VapeLog extends Model
{
    protected $fillable = [
        'puffs',
        'wattage',
    ];

    public function user(): BelongsTo
    {
        return $this->belongsTo(User::class);
    }

    public function device(): BelongsTo
    {
        return $this->belongsTo(Device::class);
    }

    public function coil(): BelongsTo
    {
        return $this->belongsTo(Coil::class);
    }

    public function juice(): BelongsTo
    {
        return $this->belongsTo(Juice::class);
    }
}
