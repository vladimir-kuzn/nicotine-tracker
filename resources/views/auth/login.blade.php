@extends('layouts.app')
@section('title', 'вход // nimo')
@section('content')
    <div class="flex h-full w-full flex-col items-center justify-center">
        <h1 class="text-6xl text-wild-sand-950 font-medium">Вход в 'nimo'</h1>
        &nbsp;<script async src="https://telegram.org/js/telegram-widget.js?7" data-telegram-login="NicotineTrackerBot" data-size="large" data-userpic="false" data-auth-url="{{ route('telegram.callback') }}" data-request-access="write"></script>
        <a href="#"
           class="mt-4 hover:underline underline-offset-2 hover:text-sky-400 transition-all">Уже есть код</a>
        <span class="mt-3">Код будет действителен 3 минуты</span>
    </div>
@endsection
