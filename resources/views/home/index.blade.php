@extends('layouts.app')
@section('title', 'главная // nimo')
@section('content')
    <div class="flex h-full w-full flex-col items-center justify-center">
        <h1 class="text-6xl text-wild-sand-950 font-medium">Добро пожаловать в 'nimo'</h1>
        <a href="#"
           class="mt-4 hover:underline underline-offset-2 hover:text-sky-400 transition-all">Просто ссылка</a>
        <span class="mt-3">Какой-то текст</span>
    </div>
@endsection
