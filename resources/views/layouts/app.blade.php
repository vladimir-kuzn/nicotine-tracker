<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=0.7">

    <title>@yield('title')</title>

    <!-- Scripts -->
    @vite('resources/js/app.js')
    <script src="//unpkg.com/alpinejs" defer></script>

    <!-- Styles -->
    @vite('resources/css/app.css')
    <style>
        @import url('https://fonts.googleapis.com/css2?family=Inter:wght@100;200;300;400;500;600;700;800;900&display=swap');

        * {
            font-family: "Inter", sans-serif
        }
    </style>
</head>
<body class="[min-height:100dvh]">
<div class="[min-height:100dvh] flex flex-col justify-between items-center">
    <div class="w-full flex justify-between h-full grow items-stretch mt-4">
        @if(!Route::is('login') & !Route::is('welcome'))
            <div class="flex justify-between min-h-screen fixed top-0 left-0 bg-white">
                <aside class="flex flex-col items-stretch justify-between w-max min-h-full my-4">
                    <div class="flex flex-col items-stretch justify-start space-y-8 mx-4">
                        <div class="flex space-x-1 justify-center w-full" title="Инструменты системного администратора">
                            <h3 class="aeza-text-4xl-bold border-zinc-200 border-2 border-b-4 border-r-4 font-semibold flex items-center justify-center rounded-xl w-[46px] h-[46px]">
                                n</h3>
                            <h3 class="aeza-text-4xl-bold border-zinc-200 border-2 border-b-4 border-r-4 font-semibold flex items-center justify-center rounded-xl w-[46px] h-[46px]">
                                i</h3>
                            <h3 class="aeza-text-4xl-bold border-zinc-200 border-2 border-b-4 border-r-4 font-semibold flex items-center justify-center rounded-xl w-[46px] h-[46px]">
                                m</h3>
                            <h3 class="aeza-text-4xl-bold border-zinc-200 border-2 border-b-4 border-r-4 font-semibold flex items-center justify-center rounded-xl w-[46px] h-[46px]">
                                o</h3>
                        </div>
                        <div class="flex flex-col space-y-3">
                            <div class="flex items-center space-x-2 mb-2">
                                <svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24"
                                     stroke-width="1.5"
                                     stroke="currentColor" class="w-6 h-6 text-wild-sand-400">
                                    <path stroke-linecap="round" stroke-linejoin="round" d="M19.5 14.25v-2.625a3.375 3.375 0 0 0-3.375-3.375h-1.5A1.125 1.125 0 0 1 13.5 7.125v-1.5a3.375 3.375 0 0 0-3.375-3.375H8.25M9 16.5v.75m3-3v3M15 12v5.25m-4.5-15H5.625c-.621 0-1.125.504-1.125 1.125v17.25c0 .621.504 1.125 1.125 1.125h12.75c.621 0 1.125-.504 1.125-1.125V11.25a9 9 0 0 0-9-9Z" />
                                </svg>
                                <h2 class="w-max aeza-text-small text-wild-sand-400">статистика</h2>
                                <hr class="h-[2px] flex-grow bg-wild-sand-400">
                            </div>
                            <a class="w-max flex aeza-text-normal text-wild-sand-900 hover:underline underline-offset-2 hover:text-sky-400 transition-all"
                               href="{{ route('home') }}">
                                <svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24"
                                     stroke-width="1.5"
                                     stroke="currentColor" class="w-6 h-6 mr-1">
                                    <path stroke-linecap="round" stroke-linejoin="round"
                                          d="M6 6.878V6a2.25 2.25 0 012.25-2.25h7.5A2.25 2.25 0 0118 6v.878m-12 0c.235-.083.487-.128.75-.128h10.5c.263 0 .515.045.75.128m-12 0A2.25 2.25 0 004.5 9v.878m13.5-3A2.25 2.25 0 0119.5 9v.878m0 0a2.246 2.246 0 00-.75-.128H5.25c-.263 0-.515.045-.75.128m15 0A2.25 2.25 0 0121 12v6a2.25 2.25 0 01-2.25 2.25H5.25A2.25 2.25 0 013 18v-6c0-.98.626-1.813 1.5-2.122"/>
                                </svg>
                                Главная
                            </a>

                            <a class="w-max flex aeza-text-normal text-wild-sand-900 hover:underline underline-offset-2 hover:text-sky-400 transition-all"
                               href="{{ route('home') }}">
                                <svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24"
                                     stroke-width="1.5"
                                     stroke="currentColor" class="w-6 h-6 mr-1">
                                    <path stroke-linecap="round" stroke-linejoin="round" d="M9.75 3.104v5.714a2.25 2.25 0 0 1-.659 1.591L5 14.5M9.75 3.104c-.251.023-.501.05-.75.082m.75-.082a24.301 24.301 0 0 1 4.5 0m0 0v5.714c0 .597.237 1.17.659 1.591L19.8 15.3M14.25 3.104c.251.023.501.05.75.082M19.8 15.3l-1.57.393A9.065 9.065 0 0 1 12 15a9.065 9.065 0 0 0-6.23-.693L5 14.5m14.8.8 1.402 1.402c1.232 1.232.65 3.318-1.067 3.611A48.309 48.309 0 0 1 12 21c-2.773 0-5.491-.235-8.135-.687-1.718-.293-2.3-2.379-1.067-3.61L5 14.5" />
                                </svg>
                                Кол-во и никотин
                            </a>

                            <a class="w-max flex aeza-text-normal text-wild-sand-900 hover:underline underline-offset-2 hover:text-sky-400 transition-all"
                               href="{{ route('home') }}">
                                <svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24"
                                     stroke-width="1.5"
                                     stroke="currentColor" class="w-6 h-6 mr-1">
                                    <path stroke-linecap="round" stroke-linejoin="round" d="M2.25 18.75a60.07 60.07 0 0 1 15.797 2.101c.727.198 1.453-.342 1.453-1.096V18.75M3.75 4.5v.75A.75.75 0 0 1 3 6h-.75m0 0v-.375c0-.621.504-1.125 1.125-1.125H20.25M2.25 6v9m18-10.5v.75c0 .414.336.75.75.75h.75m-1.5-1.5h.375c.621 0 1.125.504 1.125 1.125v9.75c0 .621-.504 1.125-1.125 1.125h-.375m1.5-1.5H21a.75.75 0 0 0-.75.75v.75m0 0H3.75m0 0h-.375a1.125 1.125 0 0 1-1.125-1.125V15m1.5 1.5v-.75A.75.75 0 0 0 3 15h-.75M15 10.5a3 3 0 1 1-6 0 3 3 0 0 1 6 0Zm3 0h.008v.008H18V10.5Zm-12 0h.008v.008H6V10.5Z" />
                                </svg>
                                Стоимость
                            </a>
                        </div>
                        <div class="flex flex-col space-y-3">
                            <div class="flex items-center space-x-2 mb-2" title="Система контроля ресурсов">
                                <svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24"
                                     stroke-width="1.5"
                                     stroke="currentColor" class="w-6 h-6 text-wild-sand-400">
                                    <path stroke-linecap="round" stroke-linejoin="round"
                                          d="M21 7.5l-2.25-1.313M21 7.5v2.25m0-2.25l-2.25 1.313M3 7.5l2.25-1.313M3 7.5l2.25 1.313M3 7.5v2.25m9 3l2.25-1.313M12 12.75l-2.25-1.313M12 12.75V15m0 6.75l2.25-1.313M12 21.75V19.5m0 2.25l-2.25-1.313m0-16.875L12 2.25l2.25 1.313M21 14.25v2.25l-2.25 1.313m-13.5 0L3 16.5v-2.25"/>
                                </svg>
                                <h2 class="w-max aeza-text-small text-wild-sand-400">предметы</h2>
                                <hr class="h-[2px] flex-grow bg-wild-sand-400">
                            </div>
                            <a class="w-max flex aeza-text-normal text-wild-sand-900 hover:underline underline-offset-2 hover:text-sky-400 transition-all"
                               href="#">
                                <svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24"
                                     stroke-width="1.5" stroke="currentColor" class="w-6 h-6 mr-1">
                                    <path stroke-linecap="round" stroke-linejoin="round" d="M11.412 15.655 9.75 21.75l3.745-4.012M9.257 13.5H3.75l2.659-2.849m2.048-2.194L14.25 2.25 12 10.5h8.25l-4.707 5.043M8.457 8.457 3 3m5.457 5.457 7.086 7.086m0 0L21 21" />
                                </svg>
                                Пачки сигарет
                            </a>
                            <a class="w-max flex aeza-text-normal text-wild-sand-900 hover:underline underline-offset-2 hover:text-sky-400 transition-all"
                               href="#">
                                <svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24"
                                     stroke-width="1.5" stroke="currentColor" class="w-6 h-6 mr-1">
                                    <path stroke-linecap="round" stroke-linejoin="round" d="m3.75 13.5 10.5-11.25L12 10.5h8.25L9.75 21.75 12 13.5H3.75Z" />
                                </svg>
                                Вейпы
                            </a>
                            <a class="w-max flex aeza-text-normal text-wild-sand-900 hover:underline underline-offset-2 hover:text-sky-400 transition-all"
                               href="#">
                                <svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24"
                                     stroke-width="1.5" stroke="currentColor" class="w-6 h-6 mr-1">
                                    <path stroke-linecap="round" stroke-linejoin="round" d="m15 11.25 1.5 1.5.75-.75V8.758l2.276-.61a3 3 0 1 0-3.675-3.675l-.61 2.277H12l-.75.75 1.5 1.5M15 11.25l-8.47 8.47c-.34.34-.8.53-1.28.53s-.94.19-1.28.53l-.97.97-.75-.75.97-.97c.34-.34.53-.8.53-1.28s.19-.94.53-1.28L12.75 9M15 11.25 12.75 9" />
                                </svg>
                                Жидкости
                            </a>
                            <a class="w-max flex aeza-text-normal text-wild-sand-900 hover:underline underline-offset-2 hover:text-sky-400 transition-all"
                               href="#">
                                <svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24"
                                     stroke-width="1.5" stroke="currentColor" class="w-6 h-6 mr-1">
                                    <path stroke-linecap="round" stroke-linejoin="round" d="M9 8.25H7.5a2.25 2.25 0 0 0-2.25 2.25v9a2.25 2.25 0 0 0 2.25 2.25h9a2.25 2.25 0 0 0 2.25-2.25v-9a2.25 2.25 0 0 0-2.25-2.25H15M9 12l3 3m0 0 3-3m-3 3V2.25" />
                                </svg>
                                Испарители/койлы
                            </a>
                        </div>
                    </div>

                    <div class="flex flex-col items-stretch justify-start space-y-8 mx-4 mt-4">
                        <div class="flex flex-col space-y-3 items-stretch">
                            <div class="flex items-center space-x-2 mb-2">
                                <svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24"
                                     stroke-width="1.5" stroke="currentColor" class="w-6 h-6 text-wild-sand-400">
                                    <path stroke-linecap="round" stroke-linejoin="round"
                                          d="M6.75 12a.75.75 0 11-1.5 0 .75.75 0 011.5 0zM12.75 12a.75.75 0 11-1.5 0 .75.75 0 011.5 0zM18.75 12a.75.75 0 11-1.5 0 .75.75 0 011.5 0z"/>
                                </svg>
                                <h2 class="w-max aeza-text-small text-wild-sand-400">прочее</h2>
                                <hr class="h-[2px] flex-grow bg-wild-sand-400">
                            </div>
                            <a class="flex aeza-text-normal text-wild-sand-900 hover:underline underline-offset-2 hover:text-sky-400 transition-all"
                               href="{{ route('styles') }}">
                                <svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24"
                                     stroke-width="1.5" stroke="currentColor" class="w-6 h-6 mr-1">
                                    <path stroke-linecap="round" stroke-linejoin="round"
                                          d="M9.53 16.122a3 3 0 00-5.78 1.128 2.25 2.25 0 01-2.4 2.245 4.5 4.5 0 008.4-2.245c0-.399-.078-.78-.22-1.128zm0 0a15.998 15.998 0 003.388-1.62m-5.043-.025a15.994 15.994 0 011.622-3.395m3.42 3.42a15.995 15.995 0 004.764-4.648l3.876-5.814a1.151 1.151 0 00-1.597-1.597L14.146 6.32a15.996 15.996 0 00-4.649 4.763m3.42 3.42a6.776 6.776 0 00-3.42-3.42"/>
                                </svg>
                                Стили сервиса
                            </a>
                            <a href="#"
                               class="pl-1 aeza-grey-button flex items-center flex-none max-w-[196px]">
                                <img src="{{ Auth::user()->photo_url }}" alt="avatar" class="w-8 h-8 rounded-full mr-2">
                                <span class="truncate">{{ Auth::user()->first_name." ".Auth::user()->last_name }}1234567890</span>
                            </a>
                            <form action="{{ route('logout') }}" method="POST">
                                @csrf
                                <button type="submit"
                                        class="text-red-500 rounded-3xl hover:text-red-300 transition-all">Выйти
                                </button>
                            </form>
                        </div>

                    </div>
                </aside>
                <div class="inline-block min-h-full w-[1px] bg-wild-sand-400"></div>
            </div>
        @endif
        {{-- "ml" = width of the upper div --}}
        <div
            class="flex flex-col items-stretch justify-between w-full @if(!Route::is('login') & !Route::is('welcome')) ml-[217px] pr-[217px] @endif">
            <div class="flex items-center justify-center w-full grow">
                @yield('content')
            </div>
            <div
                class="flex justify-between items-center space-x-8 bg-black text-white @if(!Route::is('login') & !Route::is('welcome')) -mr-[217px] @endif py-1 px-8 mt-4">
                <div>
                    @php
                        $commitHash = trim(shell_exec('git rev-parse --short HEAD'));
                    @endphp
                    <p>Текущий коммит: {{ $commitHash }}</p>
                </div>
                <div class="flex justify-center items-center space-x-2">
                    <p class="">
                        Made
                    </p>
                    <a href="https://pheo.dev" target="_blank" class="bg-white p-1 m-3 rounded-lg">
                        <h1 is="type-async" id="type-text" class="text-black font-bold">PHEO.DEV</h1>
                    </a>
                    <p>
                        in 2024
                    </p>
                </div>

            </div>
        </div>
    </div>
</div>
@stack('scripts')
</body>
</html>
