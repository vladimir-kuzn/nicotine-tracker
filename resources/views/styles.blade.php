@extends('layouts.app')
@section('title', 'стили // nimo')
@section('content')
    <div class="w-full h-full p-8 flex justify-center space-x-8">
        <div class="flex flex-col items-start space-y-4">
            <span class="aeza-text-6xl-bold">aeza-text-6xl-bold</span>
            <span class="aeza-text-6xl-medium">aeza-text-6xl-medium</span>
            <span class="aeza-text-4xl-bold">aeza-text-4xl-bold</span>
            <span class="aeza-text-4xl-medium">aeza-text-4xl-medium</span>
            <span class="aeza-text-2xl-bold">aeza-text-2xl-bold</span>
            <span class="aeza-text-2xl-medium">aeza-text-2xl-medium</span>
            <span class="aeza-text-xl-bold">aeza-text-xl-bold</span>
            <span class="aeza-text-xl-medium">aeza-text-xl-medium</span>
            <span class="aeza-text-normal">aeza-text-normal</span>
            <span class="aeza-text-small">aeza-text-small</span>
            <label>
                <input type="text" class="aeza-input" placeholder="aeza-input" value="aeza-input">
            </label>
            <label class="flex items-center space-x-1">
                <input type="checkbox" name="hidden" class="aeza-checkbox" checked/>
                <span>aeza-checkbox</span>
            </label>
            <button type="submit" class="aeza-black-button">aeza-black-button</button>
            <a href="#" class="aeza-grey-button">aeza-grey-button</a>
            <a href="#" class="aeza-stylized-button">aeza-stylized-button</a>
        </div>
        <div class="min-h-full bg-wild-sand-200 w-0.5 rounded-full"></div>
        <div class="flex flex-col space-y-2">
            <div
                class="bg-cyan-100 p-4 rounded-3xl w-[255px] h-[120px] flex flex-col justify-center">
                <p>bg-cyan-100</p>
                <p>rounded-3xl</p>
            </div>
            <div
                class="bg-wild-sand-50 p-4 rounded-xl border-2 border-black w-[255px] h-min flex flex-col">
                <p>bg-wild-sand-50</p>
                <p>rounded-xl</p>
                <p>border-2 border-black</p>
            </div>
            <div
                class="bg-wild-sand-100 p-4 rounded border border-black w-[255px] h-min flex flex-col">
                <p>bg-wild-sand-100</p>
                <p>rounded</p>
                <p>border border-black</p>
            </div>
        </div>
        <div class="min-h-full bg-wild-sand-200 w-0.5 rounded-full"></div>
        <div class="flex flex-col">
            <span class="text-6xl font-bold text-black">text-black</span>
            <span class="text-6xl font-bold text-wild-sand-900">text-wild-sand-900</span>
            <span class="text-6xl font-bold text-wild-sand-400">text-wild-sand-400</span>
            <span class="text-6xl font-bold text-red-500">text-red-500</span>
            <span class="text-6xl font-bold text-red-300">text-red-300</span>
            <span class="text-6xl font-bold text-cyan-400">text-cyan-400</span>
            <article class="prose mt-8">
                <pre><code class="language-css">/** @type {import('tailwindcss').Config} */
export default {
    content: [
        "./resources/**/*.blade.php",
        "./resources/**/*.js",
        "./resources/**/*.vue",
    ],
    theme: {
        extend: {
            colors: {
                "wild-sand": {
                    50: "#f4f4f4",
                    100: "#efefef",
                    200: "#dcdcdc",
                    300: "#bdbdbd",
                    400: "#989898",
                    500: "#7c7c7c",
                    600: "#656565",
                    700: "#525252",
                    800: "#464646",
                    900: "#3d3d3d",
                    950: "#292929",
                },
            },
        },
    },
    plugins: [],
};</code></pre>
            </article>
        </div>
    </div>
@endsection
