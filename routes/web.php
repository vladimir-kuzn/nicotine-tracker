<?php

use App\Http\Controllers\AuthController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/

Route::get('/', function () {
    return view('welcome');
})->name('welcome');

Route::middleware('guest')->group(function () {
    Route::get('/login', function (){
        return view('auth.login');
    })->name('login');
    Route::get('/login/telegram/callback', [AuthController::class, 'handleTelegramCallback'])
        ->name('telegram.callback');
});

Route::middleware('auth')->group(function () {
    Route::get('/home', function (){
        return view('home.index');
    })->name('home');

    Route::post('/logout', [AuthController::class, 'logout'])
        ->name('logout');
});


Route::get('/styles', function () {
    return view('styles');
})->name('styles');
