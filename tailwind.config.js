/** @type {import('tailwindcss').Config} */
export default {
    content: [
        "./resources/**/*.blade.php",
        "./resources/**/*.js",
        "./resources/**/*.vue",
    ],
    theme: {
        extend: {
            colors: {
                "wild-sand": {
                    50: "#f4f4f4",
                    100: "#efefef",
                    200: "#dcdcdc",
                    300: "#bdbdbd",
                    400: "#989898",
                    500: "#7c7c7c",
                    600: "#656565",
                    700: "#525252",
                    800: "#464646",
                    900: "#3d3d3d",
                    950: "#292929",
                },
            },
        },
    },
    plugins: [],
};
